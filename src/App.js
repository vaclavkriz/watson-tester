import React, { Component } from 'react';
import MediaQuery from 'react-responsive';
import { watsonBase, watsonEncodedCredentials, watsonVersion } from './config'
import {
  ThemeProvider,
  TextComposer,
  Row,
  TextInput,
  SendButton,
  IconButton,
  CloseIcon,
  AgentBar,
  Avatar,
  Column,
  Title,
  Subtitle
} from '@livechat/ui-kit';
import Message from './Messages';
import { Scrollbars } from 'react-custom-scrollbars';
import uuid from 'uuid';
const dateFormat = require('dateformat');

class App extends Component {
  state = {
    context: {},
    messages: [],
    closeColor: "#636363",
    whereToFind: undefined,
    input: undefined,
    commentW: undefined,
    lista: "hidden",
    denied: false,
    time: undefined
  }


  handleChange = (e) => {
    this.setState({
      input: e.target.value
    });
  }

  getTimeNow = () => {
    let time = new Date(Date.now());
    const formDate = dateFormat(time, 'HH:MM')
    this.setState({
      time: formDate
    })

  }

  callWatson = () => { // TODO: handlovani local storage 
    const text = this.state.input;
    fetch(
      `${watsonBase}/message?version=${watsonVersion}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          Authorization: `Basic ${watsonEncodedCredentials}`,
        },
        body: JSON.stringify({
          input: { text },
          context: this.state.context,
        }),
      },
    )
      .then(res => res.json())
      .then(response => {
        console.log(response);
        if (this.state.messages.length === 0) {
          this.setState({
            border: "2px solid black"
          })
        }
        this.getTimeNow();

        this.setState({
          context: response.context,
          messages: [...this.state.messages, (<Message key={uuid()} input={response.input.text} output={response.output.text.join(' ')}
            time={this.state.time} />)],
          closeColor: "red"

        })
      })
      .catch(err => {
        console.log(err);
      })


  }

  goBottom = () => {
    const { scrollbar } = this.refs;
    scrollbar.scrollToBottom();
  }

  componentWillMount = () => {

    if (localStorage.getItem('messages') && localStorage.getItem('context')) {
      const pole = JSON.parse(localStorage.getItem('messages'));
      this.setState({
        context: JSON.parse(localStorage.getItem('context'))
      })
      console.log(pole);
      let tempPole = []
      for (let i = 0; i < pole.length; i++) {
        tempPole[i] = (<Message key={uuid()} input={pole[i].props.input} output={pole[i].props.output} time={pole[i].props.time} />)
      }

      this.setState({
        messages: tempPole,
        denied: false
      })
      tempPole = [];

    }
  }

  componentDidUpdate = () => {

    if (this.state.denied) {
      localStorage.clear();
    }
    this.goBottom();

  }

  componentWillUpdate = (nextProps, nextState) => {

    if (this.state.denied === false) {

      localStorage.setItem('messages', JSON.stringify(nextState.messages));
      localStorage.setItem('context', JSON.stringify(nextState.context));
    }
    else {
      localStorage.clear()
    }
  }


  render() {
    const stylemsg = {
      border: '2px solid black',
      borderRadius: '5px',
      width: "40%",
      maxHeight: "500px",
      marginBottom: "10px",
      backgroundColor: "#E8E8E8",
      borderTopLeftRadius: "0px",
      borderTopRightRadius: "0px",
      borderTop: "hidden",
    }
    const stylemsgM = {
      border: '2px solid black',
      borderTop: "hidden",
      borderRadius: '5px',
      width: "98%",
      maxHeight: "1000px",
      marginBottom: "10px",
      backgroundColor: "#E8E8E8",
      borderTopLeftRadius: "0px",
      borderTopRightRadius: "0px"
    }
    return (




      <div style={{

        position: "fixed",
        top: "0",
        bottom: "0",
        right: "0",
        left: "0",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        backgroundColor: "white",

      }}>


        <MediaQuery maxDeviceWidth={1224}>
          <img src="https://www.specialistsgrid.com/skin/frontend/default/pagayo-theme-001/images/logo.png" alt="logo" />


          <div style={{
            border: "2px solid black", borderBottom: "hidden", width: "98%", borderRadius: "5px", borderBottomLeftRadius: "0px",
            borderBottomRightRadius: "0px", textAlign: "center", color: "white", backgroundColor: "#4788ef", padding: "5px 0px 5px 0px", marginTop: "10px"
          }}>

            Messenger
    <ThemeProvider>
              <AgentBar>
                <Avatar imgUrl="https://achievement-images.teamtreehouse.com/badge_chatbot-watson-apis_stage1.png" />
                <Column>
                  <Title style={{ color: "black" }}>{'Watson'}</Title>
                  <Subtitle style={{ color: "black" }}>{'Assistent'}</Subtitle>
                </Column>
              </AgentBar>
            </ThemeProvider>
          </div>
          <Scrollbars style={stylemsgM}{...this.props} ref="scrollbar">

            {this.state.messages}
          </Scrollbars>

          <div style={{ width: "98%", border: "2px solid black", borderRadius: "5px" }} >
            <ThemeProvider>
              <TextComposer style={{ backgroundColor: "white" }} value={this.state.input} onChange={this.handleChange} onButtonClick={this.callWatson} onKeyDown={(e) => {
                if (e.keyCode === 13) {
                  this.callWatson()
                }
              }}>
                <Row align="center">
                  <TextInput style={{ backgroundColor: "white", color: "black" }} />
                  <SendButton color="#4788ef" />

                  <IconButton onClick={() => {

                    this.setState({
                      context: {},
                      messages: [],
                      denied: true,
                    })


                  }}>
                    <CloseIcon color={this.state.closeColor} onMouseOver={() => {
                      this.setState({
                        closeColor: "red"
                      })
                    }}
                      onMouseOut={() => {
                        this.setState({
                          closeColor: "#636363"
                        })
                      }}


                    />
                  </IconButton>

                </Row>
              </TextComposer>
            </ThemeProvider>
          </div>

        </MediaQuery>



        <MediaQuery minDeviceWidth={1224}>
          <img src="https://www.specialistsgrid.com/skin/frontend/default/pagayo-theme-001/images/logo.png" alt="logo" />

          <div style={{
            border: "2px solid black", borderBottom: "hidden", width: "40%", borderRadius: "5px", borderBottomLeftRadius: "0px",
            borderBottomRightRadius: "0px", textAlign: "center", color: "white", backgroundColor: "#4788ef", padding: "5px 0px 5px 0px", marginTop: "10px"
          }}>

            Messenger
    <ThemeProvider>
              <AgentBar>
                <Avatar imgUrl="https://achievement-images.teamtreehouse.com/badge_chatbot-watson-apis_stage1.png" />
                <Column>
                  <Title style={{ color: "black" }}>{'Watson'}</Title>
                  <Subtitle style={{ color: "black" }}>{'Assistent'}</Subtitle>
                </Column>
              </AgentBar>
            </ThemeProvider>
          </div>


          <Scrollbars style={stylemsg}{...this.props} ref="scrollbar">
            {this.state.messages}
          </Scrollbars>

          <div style={{ width: "40%", border: "2px solid black", borderRadius: "5px" }} >
            <ThemeProvider>
              <TextComposer value={this.state.input} onChange={this.handleChange} onButtonClick={this.callWatson} onKeyDown={(e) => {
                if (e.keyCode === 13) {
                  this.callWatson()
                }
              }}>
                <Row align="center">
                  <TextInput />
                  <SendButton />

                  <IconButton onClick={() => {

                    this.setState({
                      context: undefined,
                      messages: [],
                      denied: true,
                    })


                  }}>
                    <CloseIcon color={this.state.closeColor} onMouseOver={() => {
                      this.setState({
                        closeColor: "pink"
                      })
                    }}
                      onMouseOut={() => {
                        this.setState({
                          closeColor: "#636363"
                        })
                      }}


                    />
                  </IconButton>

                </Row>
              </TextComposer>
            </ThemeProvider>
          </div>

        </MediaQuery>

      </div>

    );
  }
}

export default App;
