export const watsonBase = "https://gateway-lon.watsonplatform.net/assistant/api/v1/workspaces/784ea5cb-7225-427b-81dc-04780a428941";
export const watsonEncodedCredentials = "YXBpa2V5Oll5SU83RjNDTTk2YVRHaE5SUVlKWVRjYS05eXduVTFaQ25faS1kbG51cl9r";
export const watsonVersion = "2018-09-20";