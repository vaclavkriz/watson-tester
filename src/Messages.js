import React, { Component } from 'react';
import {  Avatar } from '@livechat/ui-kit';
import MediaQuery from 'react-responsive';


class Message extends Component {




    render(){
      return (      
        
        <div style={{}}>
        <MediaQuery maxDeviceWidth={1224}>
        <div style={{padding:"10px",width:"40%",marginLeft:"50%", marginTop:"10px",backgroundColor:"#FF6500",fontSize:20,borderRadius:5,color:"white",borderBottomRightRadius:"0px"}}>
        {this.props.input}
        
        </div>
        <span style={{float:"right",marginRight:"5%",fontFamily:"Arial",fontSize:"12px"}}>{this.props.time}</span>

        
        <Avatar style={{float:"left",marginTop:"15.3px"}} size="30px" imgUrl="https://achievement-images.teamtreehouse.com/badge_chatbot-watson-apis_stage1.png"  />
        
        
        <div style={{color:"#484848",padding:"10px",marginLeft:"10%",marginTop:"17px",marginBottom:"5px",width:"40%",backgroundColor:"#FFFFFF",fontSize:20,borderRadius:5,borderBottomLeftRadius:"0px"}}>
        {this.props.output}
        <div style={{fontSize:15}}>
        <span style={{color:"red"}}>
          {this.props.commentW}
        </span>
        {this.props.comment}
        </div>
        </div>
        <span style={{float:"left",marginLeft:"10%",fontFamily:"Arial",fontSize:"12px"}}>{this.props.time}</span>
        </MediaQuery>



        <MediaQuery minDeviceWidth={1224}>
        <div style={{padding:"10px",width:"40%",marginLeft:"45%", marginTop:"10px",backgroundColor:"#FF6500",fontSize:20,borderRadius:5,color:"white",borderBottomRightRadius:"0px"}}>
        {this.props.input}
        
        </div>
        <span style={{float:"right",marginRight:"12.5%",fontFamily:"Arial",fontSize:"12px"}}>{this.props.time}</span>

        
        <Avatar style={{float:"left",marginTop:"15.3px",marginLeft:"40px"}} size="30px" imgUrl="https://achievement-images.teamtreehouse.com/badge_chatbot-watson-apis_stage1.png"  />
        
        
        <div style={{color:"#484848",padding:"10px",marginLeft:"10%",marginTop:"17px",marginBottom:"5px",width:"40%",backgroundColor:"#FFFFFF",fontSize:20,borderRadius:5,borderBottomLeftRadius:"0px"}}>
        {this.props.output}
        <div style={{fontSize:15}}>
        <span style={{color:"red"}}>
     
        </span>
      
        </div>
        </div>
        <span style={{float:"left",marginLeft:"10%",fontFamily:"Arial",fontSize:"12px"}}>{this.props.time}</span>

        </MediaQuery>
       </div>
       
      );
    }
  }
  
  export default Message;